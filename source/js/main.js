/**
 * Created by Mohammad Atif on 6/19/2016.
 */

(function () {
    $(document).ready(function () {
        'use strict';


        //set defaults
        var lang = ($('html').attr('lang') || 'ar').trim().toLowerCase();

        //set default locale for moment.js library
        moment.locale(lang);
        
        // get news once the document loads
        getNews(lang);
        
    });

    function getNews(lang){
        //var data_url = "https://www.gso.org.sa/gso-website/gso-website/media-"+lang+"/media-connect/news?json=yes";
        var data_url = '../json/news_ar.json';

        $.getJSON(data_url, function (data) {
            
            var htmlString = '';

            $.each(data,function (key, value) {
                htmlString += '<div class="newsList">';
                htmlString += '<a href="http://gso.org.sa'+value.path+'" target="_self">';
                htmlString += '<span class="content">'+value.title+'</span>';
                htmlString += '<span class="date">'+moment(value.date).format('DD-MMM-YYYY')+'</span>';
                htmlString += '<i class="arrow glyphicon glyphicon-chevron-left"></i>'
                htmlString += '</a>';
                htmlString += '</div>';
            });

            $('#homeNewsList').html(htmlString);



        });
    }
    
    
})();