/**
 * Created by Mohammad Atif on 6/19/2016.
 */

// include gulp and plugins
var
    gulp = require('gulp'),
    newer = require('gulp-newer'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    htmlclean = require('gulp-htmlclean'),
    size = require('gulp-size'),
    uglify = require('gulp-uglify'),
    pleeease = require('gulp-pleeease'),
    preprocess = require('gulp-preprocess'),
    less = require('gulp-less'),
    runsequence = require('run-sequence'),
    browsersync = require('browser-sync'),
    pkg = require('./package.json');


//dev build flag
var
    devBuild = ((process.env.NODE_ENV || 'development').trim().toLowerCase() !== 'production');


// file locations
var
    source = 'source/',
    //dest = (devBuild)?'build/':'release/',
    dest = 'build/';

    html = {
        in:source + '*.html',
        watch:[source + '*.html', source + 'templates/**/*'],
        out:dest,
        context:{
            devBuild:devBuild,
            author:pkg.author,
            version:pkg.version
        }
    },

    css={
        in: source + 'less/*.less',
        watch: [source + 'less/**/*'],
        out: dest + 'css/',
        lessOpts:{},
        pleeeaseOpts:{
            minifier: !devBuild
        }
    },
    
    js = {
        in: source + 'js/**/*',
        watch:[source + 'js/**/*.js'],
        out: dest + 'js/'
    },

    browserSyncOpt = {
        server: {
            baseDir: dest,
            index: 'index.html'
        },
        open: false, // don't open by default
        notify:true, //browerSync show message whenever it does something
        port:3005
    },

    fonts = {
        in: source + 'fonts/',
        out: dest
    },

    images = {
        in: source + 'images/**/*',
        out: dest + 'images/'
    };


console.log('Building Package '+ pkg.name +' version' + pkg.version + ' for ' + (devBuild?'development':'production'));

//clean the build folder
gulp.task('clean',function () {
    del([
        dest + '*'
    ]) ;
});


gulp.task('copy', function () {
    return gulp.src([

        // Copy all files
        source + '/**/*',

        // Exclude the following files
        // (other tasks will handle the copying of these files)
        '!' + source + 'less/**/*',
        '!' + source + 'templates/**/*',
        '!' + source + 'images/**/*',
        '!' + source + 'js/**/*',
        '!' + source + 'fonts/**/*',
        '!' + source + 'index.html'

    ], {

        // Include hidden files by default
        dot: true

    }).pipe(gulp.dest(dest));
});


//clean the folder required for development management only
gulp.task('remove:misc',function () {
    del([
        dest + 'less',
        dest + 'templates'
    ]) ;
});


//build HTML files
gulp.task('html',function () {

    var page =  gulp.src(html.in).pipe(preprocess({context:html.context}));

    if(!devBuild){
        page = page
            .pipe(size({title: 'HTML files before compression'}))
            .pipe(htmlclean())
            .pipe(size({title: 'HTML files after compression'}));
    }

    return page.pipe(gulp.dest(html.out));
});

//manage images
gulp.task('images',function () {
   return gulp.src(images.in)
       .pipe(newer(images.out))
       .pipe(imagemin())
       .pipe(gulp.dest(images.out));
});


//copy fonts
gulp.task('fonts',function () {
   return gulp.src(fonts.in)
       .pipe(newer(fonts.out))
       .pipe(gulp.dest(fonts.out));
});


//compile LESS
gulp.task('less',function () {
   return gulp.src(css.in)
       .pipe(less())
       .pipe(size({title: 'CSS files before compression'}))
       .pipe(pleeease(css.pleeeaseOpts))
       .pipe(size({title: 'CSS files after compression'}))
       .pipe(gulp.dest(css.out))
       .pipe(browsersync.reload({stream:true}));
});

//compile js
gulp.task('js',function () {
    var
        page = gulp.src(js.in);
        if(!devBuild){
            page = page
                .pipe(size({title:'js files before compression'}))
                .pipe(uglify())
                .pipe(size({title:'js files after compression'}));
        }

    return page.pipe(gulp.dest(js.out));
});

//browser sync
gulp.task('browsersync', function () {
    browsersync(browserSyncOpt);
});



/* Main Tasks */

//build task
gulp.task('build',function () {
    runsequence(
        'clean',
        'copy',
        'remove:misc',
        ['html', 'images', 'js', 'less', 'fonts'],
        'browsersync'
    )
});

// default task
gulp.task('default', ['build'] ,function () {

    if(devBuild){
        //html changes
        gulp.watch(html.watch, ['html', browsersync.reload]);

        //image changes
        gulp.watch(images.in, ['images']);

        //font changes
        gulp.watch(fonts.in, ['fonts']);

        //LESS changes
        gulp.watch(css.watch, ['less']);

        //javascript changes
        gulp.watch(js.watch, ['js'], browsersync.reload);
    }

});
